#!/bin/sh

BIN='hordewars'
TYPE='AUGH'

read -p 'debug? [Y/n]: ' DEBUG
read -p 'clean? [y/N]: ' CLEAN

if [ "$CLEAN" = 'y' ]; then
	echo 'Purging old build data'
	cd engine
	make clean
	cd ../game
	make clean
	cd ..
	if [ -d 'build' ]; then
		rm -r 'build'
	fi
fi

if [ "$DEBUG" = 'n' ]; then
	TYPE='release'
else
	TYPE='debug'
fi

echo "Building $TYPE build..."

echo 'Building engine'
cd engine
make $TYPE

echo 'Building game'
cd ../game
make $TYPE

cd ..
echo 'Generating build'
cp -r 'resources' 'build'
cp "game/$BIN" 'build/.'

if [ "$TYPE" = 'debug' ]; then
	cp 'game/util/run_with_asan.sh' 'build/.'
fi

echo 'Done'

